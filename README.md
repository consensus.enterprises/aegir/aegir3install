Aegir 3 Installation From Git With Ansible
==========================================

1. On a newly-provisioned Ubuntu LTS VM, as a user with sudo privileges:

```
$ git clone https://gitlab.com/consensus.enterprises/aegir/aegir3install
$ cd aegir3install
```

2. Edit aegir3.yaml and fill in `mysql_root_password` and `aegir_frontend_url` variables. Optionally, also edit any other variables (`aegir_admin_email`, and so on) to taste.

3. Download dependencies and install Aegir 3:

```
$ ./aegir3up.sh 
```

--Dan Friedman (@llamech), Consensus Enterprises, continued from work started at https://github.com/platypustheory/aegirinstall

